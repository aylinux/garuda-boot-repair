#/*****************************************************************************
# * garuda-boot-repair.pro
# *****************************************************************************
# * Copyright (C) 2014 MX Authors
# *
# * Authors: Adrian
# *          MX Linux http://mxlinux.org
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * Garuda Boot Repair is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with Garuda Boot Repair.  If not, see <http://www.gnu.org/licenses/>.
# **********************************************************************/

#-------------------------------------------------
#
# Project created by QtCreator 2014-04-02T18:30:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = garuda-boot-repair
TEMPLATE = app

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

# Disable QDebug on Release build
#CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Unused not working
#CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR

        help.path=$$PREFIX/share/doc/garuda-boot-options/help/
        help.files=help/garuda-boot-repair.html
		 
        icons.path = $$PREFIX/share/icons/hicolor/scalable/apps/
        icons.files = garuda-boot-repair.png
        
        desktop.path = $$PREFIX/share/applications/
        desktop.files = "garuda-boot-repair.desktop"

        INSTALLS += target  desktop help icons
}

SOURCES += main.cpp \
    mainwindow.cpp \
    about.cpp \
    cmd.cpp

HEADERS  += \
    version.h \
    mainwindow.h \
    about.h \
    cmd.h

FORMS    += \
    mainwindow.ui

TRANSLATIONS += translations/garuda-boot-repair_am.ts \
                translations/garuda-boot-repair_ar.ts \
                translations/garuda-boot-repair_bg.ts \
                translations/garuda-boot-repair_ca.ts \
                translations/garuda-boot-repair_cs.ts \
                translations/garuda-boot-repair_da.ts \
                translations/garuda-boot-repair_de.ts \
                translations/garuda-boot-repair_el.ts \
                translations/garuda-boot-repair_es.ts \
                translations/garuda-boot-repair_et.ts \
                translations/garuda-boot-repair_eu.ts \
                translations/garuda-boot-repair_fa.ts \
                translations/garuda-boot-repair_fi.ts \
                translations/garuda-boot-repair_fil_PH.ts\
		translations/garuda-boot-repair_fr.ts \
                translations/garuda-boot-repair_he_IL.ts \
                translations/garuda-boot-repair_hi.ts \
                translations/garuda-boot-repair_hr.ts \
                translations/garuda-boot-repair_hu.ts \
                translations/garuda-boot-repair_id.ts \
                translations/garuda-boot-repair_is.ts \
                translations/garuda-boot-repair_it.ts \
                translations/garuda-boot-repair_ja.ts \
                translations/garuda-boot-repair_kk.ts \
                translations/garuda-boot-repair_ko.ts \
                translations/garuda-boot-repair_lt.ts \
                translations/garuda-boot-repair_mk.ts \
                translations/garuda-boot-repair_mr.ts \
                translations/garuda-boot-repair_nb.ts \
                translations/garuda-boot-repair_nl.ts \
                translations/garuda-boot-repair_pl.ts \
                translations/garuda-boot-repair_pt.ts \
                translations/garuda-boot-repair_pt_BR.ts \
                translations/garuda-boot-repair_ro.ts \
                translations/garuda-boot-repair_ru.ts \
                translations/garuda-boot-repair_sk.ts \
                translations/garuda-boot-repair_sl.ts \
                translations/garuda-boot-repair_sq.ts \
                translations/garuda-boot-repair_sr.ts \
                translations/garuda-boot-repair_sv.ts \
                translations/garuda-boot-repair_tr.ts \
                translations/garuda-boot-repair_uk.ts \
                translations/garuda-boot-repair_zh_CN.ts \
                translations/garuda-boot-repair_zh_TW.ts

RESOURCES += \
    images.qrc

